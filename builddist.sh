#!/usr/bin/env bash

vers=$(git describe --tags --always --dirty --match=v* 2> /dev/null || \
	   cat $(pwd)/.version 2> /dev/null || echo v0)
VERSION="${vers#v}"

set -e
sed -e "s!@VERSION@!${VERSION}!g" \
    src/components/Version.vue.in > src/components/Version.vue

npm run build


name=$(jq -r -M '.name' package.json)

echo "${name}_${VERSION}"
cd dist
tar -c -v -z -f ../${name}_${VERSION}.tar.gz *
cd -

export VERSION
nfpm pkg --target pdui_${VERSION}_any.deb
