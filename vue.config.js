module.exports = {
    publicPath: './',
    indexPath: 'pdu.html',
    chainWebpack: config => {
        config
            .plugin('html')
            .tap(args => {
                args[0].title = 'inVADER PDU'
                return args
            })
      }
}
